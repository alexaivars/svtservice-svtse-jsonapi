const lru = require("lru-cache");
const state = lru(process.env.LRU_MAX_ITEMS || 500000);
const memory = {
	set: (key, value, ttl, done) => { setImmediate(() => done(null, state.set(key, value, ttl * 1000))); },
	get: (key, done) => { setImmediate(() => done(null, state.get(key))); },
	dump: (done) => { setImmediate(() => done(null, state.dump().map(item => {return { key: item.k, ttl: Math.round((item.e - Date.now())/1000) };}))); },
	name: 'memory'
}
module.exports = memory;
