const trace = require('debug')('fn:trace');
const async = require('async');
const http = require('./http');
const execute = require('./execute');
const queue = require('./queue');
const cache = require('./cache');
const actions = (() => {
	switch(process.env.CACHE_TYPE) {
		case 'none':
			return [http];
		case 'redis':
		case 'persistent':
			return [cache(require('./persistent')), http];
		case 'memory':
			return [cache(require('./memory')), http];
		default:
			return [cache(require('./memory')), cache(require('./persistent')), http];
	}
})();

const fetch = (url, done) => {
	async.waterfall(
		[
			next => next(null, url, null, null)
		].concat(actions.map(execute)),
		(err, key, value) => {
			trace(err, key, value);
			done(err, value);
		}
	);
};


const queuedFetch = queue(fetch);

function createFetcher(/*options*/) {

	const options = Object.freeze(arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0]);
	const api = options.api;
	const hostname = options.hostname;

	return function fetcher(path, done){
		return queuedFetch({ url:`${api}${path}`, headers:{ 'x-forwarded-host':hostname } }, done);
	}
}

module.exports = createFetcher;
