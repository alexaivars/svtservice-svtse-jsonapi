function jsonError(status, title, url, sourceCode, sourceError) {
	const err = {
		status: status.toString(),
		title: title,
		source: { parameter: url },
		meta: {
			source: {
				code: sourceCode? sourceCode.toString():null,
				path: url,
				error: sourceError	
			}
		}
	}
	
	if(!sourceCode) {
		delete err.meta.source.code;
	}

	if(!sourceError) {
		delete err.meta.source.error;
	}

	return Object.assign({}, { errors: [err] });
}

module.exports = jsonError;
