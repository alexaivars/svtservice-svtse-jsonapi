const trace = require('debug')('fn:trace');
const execute = action => {
	trace('execute: ', action.name);
	return (key, value, prev, next) => {
		if(value) {
			trace('pass: ', action.name);
			next(null, key, value, null);
		} else {
			action.get(key, (err, value) => {
				if (value){
					trace('hitt: ', action.name);
					if(prev) { 
						trace('prev: ', action.name);
						prev(key, value); 
					}
					next(null, key, value, null);
				} else {
					trace('miss: ', action.name);
					next(null, key, value, (key, value) => {
						if(prev) { prev(key, value); }
						action.set(key, value);	
					});
				}
			});
		}
	}	
}

module.exports = execute;
