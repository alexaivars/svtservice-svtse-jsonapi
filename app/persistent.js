const error = require('debug')('jsonapi:persistent:error');
const redis = require('redis');
const redisConfig = { enable_offline_queue: false };
const redisURL = process.env.REDIS_URL || process.env.REDISTOGO_URL;
const redisCreate = () => {
	if (redisURL) {
		return redis
						.createClient(redisURL, redisConfig)
						.on("error", error);
	} else {
		return redis
						.createClient(redisConfig)
						.on("error", error);
	}
}
const state = redisCreate();
const persistent = {
	set: (key, value, ttl, done) => state.setex(key, ttl, value, done),
	get: (key, done) => state.get(key, done),
	dump: (done) => setImmediate(() => done(null, [])),
	name: 'persistent'
}
module.exports = persistent; 
