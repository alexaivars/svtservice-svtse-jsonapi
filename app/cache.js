const ping = require('debug')('cache:perf');
const error = require('debug')('jsonapi:cache:error');
const key = req => req.headers? req.url + '#' + req.headers['x-forwarded-host'] : req.url; 
const noop = () => {};
const parse = (val) => {
	try {
		return JSON.parse(val);
	} catch (e) {
		error('could not parse %s', val);
		return null;
	}
};

const cache = (state) => {
	return {
		get: (req, done) => {
			state.get(key(req), (err, body) => {
				if(err) { error(err); }
				if(!body) { return done(null, null); }
				ping("CACHE_HIT");		
				return done(null, parse(body) ) 
			});
		},
		set: (req, value, done) => {
			if(value && value.errors) {
				if(typeof done === 'function') { return done(null, value); }
			} else {
				const ttl = Math.max(0, parseInt((value.meta || {}).maxage || '0', 10));
				if(ttl === 0) {
					if(typeof done === 'function') { done(null, value); }
					return;
				} else {
					state.set(key(req), JSON.stringify(value), ttl, done? () => done(null, value) : noop);
				}
			}
		},
		dump: state.dump,
		name: 'cache-' + state.name
	}
}

module.exports = cache;
