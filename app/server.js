if(process.env.NEW_RELIC_ENABLED) {
	require('newrelic');
}

const error = require('debug')('jsonapi:server:error');
const debug = require('debug')('jsonapi:server:debug');
const info = require('debug')('jsonapi:server:info');
const app = require('express')();
const port = process.env.PORT || 7000;
const version = require('../package').version;
const name = require('../package').name;
const ok = Object.freeze({ "type": "health", "id": "status", "attributes": { "level": "\ud83d\udcaa", "version": version } });
const humans = [{ "type": "humans", "id": "1", "attributes": { "name": "alex aivars", "twitter": "@alexaivars", "totem": "\uD83D\uDC35" } }, { "type": "humans", "id": "2", "attributes": { "name": "jens tinfors", "twitter": "@jensa", "totem": "\uD83D\uDC36" } }];
const handler = require('./handler');
const memory = require('./memory');
const favicon = require('serve-favicon');

app.disable('x-powered-by');

app.use(favicon(__dirname + '/favicon.ico'));

app.use(function(req, res, next) {
	res
		.header('Access-Control-Allow-Origin', '*')
		.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		.header('X-Service-Name', name)
		.header('X-Service-Version', version);
  return next();
});

app.get('/health', (req, res) => res.json({ "data":[ok] }));
app.get('/health/status', (req, res) => res.json({ "data":[ok] }));
app.get('/humans/:id?', (req, res) => {
	return res
		.header('Cache-Control', 'max-age=31536000')
		.status(202)
		.json({
			"data": !req.params.id? humans : humans.filter(item => item.id === req.params.id) 
		});
});

app.get('/status/cache', (req, res) => {
	const status = { data: [] }
	if(memory.dump) {
		return memory.dump((err, value) => {
			status.data = [{
				type: 'status',
				id: 'cache',
				attributes: {
					memory: value
				}
			}];
			return res.json(status);
		});
	}
	return res.json(status);
});

app.get('/:type/:id', handler);

app.get('/:type/:id/links/:link', handler);

app.get('/:type', handler);

app.use('*', (req, res) => res.status(404).json({ errors: [{ status: "404", title: "Not Found", detail: '\ud83c\udf0b' }] }));

// default error handler must have 4 arguments in express
// or it will silently ignore our default/final handler.
// Eslint however thinks we are defining unessary variables  
app.use((err, req, res, next) => { //eslint-disable-line no-unused-vars
	if (Array.isArray(err)) {
		const status = err.reduce((memo, err) => Math.max(memo, err.status || 0), 404);
		debug('Invalid response from upstream server : %o', err);	// 502 		
		return res.status(status).json({ errors: err });
	} else {
		error('Unexpected Error : %o', err);	
		return res.status(500).json({ errors: [{
			status: "500",
			title: "Unexpected Error",
			detail: "The server encountered an unexpected condition which prevented it from fulfilling the your humble request",
			meta: { source: { url: req.url, error: err } }
		}] });
	}
});

const server = app.listen(port, function () {
		const host = server.address().address;
		const port = server.address().port;
		info('jsonapi app listening at http://%s:%s', host, port);
});
