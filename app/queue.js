const queue = fn => {
	const pending = new Map();
	return function (/*...args, done*/) {
		const args = Array.prototype.slice.call(arguments, 0, -1);
		const done = Array.prototype.slice.call(arguments, -1).pop();
		const key = JSON.stringify(args);
		
		if(pending.has(key)) {
			return pending.get(key).push(done);
		}

		pending.set(key, [done]);
		try {
			fn.apply(fn, args.concat((err, value) => {
				const resolve = pending.get(key);
				pending.delete(key);
				resolve.forEach(done => setImmediate(() => done(err, value)));
			}));
		} catch (err) {
			pending.delete(key);
			throw err;
		}
	}
};
queue.name = 'queue';

module.exports = queue;
