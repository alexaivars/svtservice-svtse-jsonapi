"use strict";

const async = require('async');
const error = require('debug')('jsonapi:crawler:error');
const debug = require('debug')('jsonapi:crawler:debug');
const flatten = arr => arr.reduce((result, value) => result.concat(value), []);

module.exports = function api(fetcher) {
	return (fetch => {

		const trim = (path) => {
			return path.replace(/(:?\?|&)include=[^&]+?$|include=[^&]*&/gmi, '');
		}
		
		const url = data => data.links?data.links.self:`/${data.type}/${data.id}`;
		
		const resolve = async.ensureAsync((related, included, done) => {
			let length = included.length;
			// Check the requested resouce allready has been resolved.
			while (length--) {
				if(included[length].id === related.id && included[length].type === related.type) {
					return done(null, [].concat(included[length]));
				}
			}
			// TODO: handle resource or data is null
			const source = url(related)
			return fetch(trim(source), (err, resource) => {
				if(resource.errors) {
					error('Failed to fetch include %s', source)		
					try {
						debug('Recived resource error %s', JSON.stringify(resource));
					} catch (e) { debug('Recived resource error %s', e); }
				}
				return done(resource.errors || null, resource.data);
			});
		});
		
		const merge = (included, change) => {
			if(change === undefined || change.length === 0) return included;
			change.forEach(item => {
				const ids = included.map(item => item.id);
				if(item && ids.indexOf(item.id) < 0) {
					included.push(item);
				}
			});
		}

		const next = (path, name, included) => {
			return (json, done) => {
				setTimeout(() => {
					const root = Object.assign({}, json);
					const errors = [];
					return async.reduce(json.data, [], (memo, item, done) => {
						
						if(!item || !item.relationships || !item.relationships[name]) {
							return done(null, memo);
						}
					
						return async.map(
							[].concat(item.relationships[name].data),
							(resource, done) => resolve(resource, included, (err, response) => {
								if(err) {
									err.forEach(item => {
										const obj = Object.assign({}, item);
										obj.source = Object.assign({}, obj.source, { pointer: `/${path.replace(/\./g, '/')}/${resource.id}` });
										errors.push(obj)
									});
								}
								done(null, response);
							}),
							(err, result) => { 
								return done(err, memo.concat(flatten(result)));
							}
						)
					}, (err, data) => {
						// if(err) return done(err, root);
						if(errors.length > 0) {
							root.meta = Object.assign({ errors:[] }, root.meta);
							root.meta.errors = root.meta.errors.concat(errors);
						}
						root.data = data;
						done(null, root);
					});
				});
			};
		}
		
		const createWalker = (path, included) => async.compose.apply(null, path.split('.').reverse().map(name => next(path, name, included)));

		const include = (json, path, included, done) => {
			const root = Object.assign({ data:[] }, json);
			return createWalker(path, included)(
				Object.freeze(json),
				(err, resolved) => {
					// if(err)	return done(null, { errors:err });
					if(resolved.meta && resolved.meta.errors) {
						root.meta = Object.assign({ errors: [] }, root.meta);
						root.meta.errors = root.meta.errors.concat(resolved.meta.errors);
					}
					merge(included, resolved.data)
					done(null, root);
				}
			);
		}

		const compound = (path, includes, done) => {
			if((includes||[]).length === 0) return fetch(trim(path), done);
			const included = [];		
			fetch(trim(path), (err, root) => {
				if(root.errors) { return done(null, root); }
				return async.map(
					includes,
					(path, cb) => {
						return include(root, path, included, cb);
					},
					(err, results) => {
						const errors = results.reduce((memo, item) => (item.meta || {}).errors ? memo.concat(item.meta.errors) : memo, []);
						const result = Object.assign(
								{ data:[] },
								root,
								{ included: included }
							);

						if(errors.length > 0) {
							result.meta = { failed: errors };
						}
							
						return done(null, result);

					}
				); 
			});	
		}
	
		return {
			fetch: fetch,
			fetchCompound: compound, 
			include: include
		}

	})(fetcher);
}
