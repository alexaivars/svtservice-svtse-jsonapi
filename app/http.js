const HTTPStatus = require('http-status');
const debug = require('debug')('jsonapi:http:debug');
const perf = require('debug')('jsonapi:http:perf');
const ping = require('debug')('cache:perf');
const isDocument = require('jsonapi-util').isDocument;
const jsonError = require('./jsonError');
const noop = () => {};
const safeParse = (val) => {
	try {
		return JSON.parse(val)
	} catch (e) {
		return val;
	}
}
const DEFAULT_MAX_AGE = process.env.API_DEFAULT_MAX_AGE || 123;
const http = (() => {
	const state = require('request');
	return {
		get: (req, done) => {
			ping("CACHE_MISS");
			const start = (new Date()).getTime();
			debug(`request url=${req.url}`);
			state.get(req, (error, response, body) => {
				perf("requested resource in %sms url=%s", (new Date()).getTime() - start, req.url);
				if (error) {
					debug("request error %o", error);
					return done(error, jsonError(500, 'Network error', req.url, undefined, error.message));
				} else if(response.statusCode.toString() === '200') {
						if(isDocument(safeParse(body)) === false) {
							return done(null, jsonError(415, 'Unsupported Media Type', req.url, response.statusCode, safeParse(body)));
						}
						const maxage = parseInt(((response.headers['cache-control'] || '').match(/max-age=(\d+)/i)||['', DEFAULT_MAX_AGE])[1], 10);
						return done(null, Object.assign({ meta: { maxage: maxage.toString() } }, safeParse(body)), maxage);
				} else {
					return done(null, jsonError(response.statusCode, HTTPStatus[response.statusCode], req.url, response.statusCode, safeParse(body)));
				}
			})
		},
		set: noop
	}
})();
http.name = 'http';
module.exports = http;
