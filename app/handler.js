const url = require('url');
const qs = require('qs');
const compose = require('./compose');
const crawler = require('./crawler'); 
const queue = require('./queue');
const fetcher = require('./createFetcher'); 
const backend = compose(crawler, fetcher);
const port = process.env.PORT || 7000;
const perf = require('debug')('jsonapi:handler:perf');

const fetch = queue((req, done) => {
	const options = Object.freeze({
		api: process.env.API || 'https://api.svt.se/svtse',
		hostname: req.headers['x-forwarded-host'] || process.env.HOSTNAME || (port? `localhost:${port}` : `localhost`)
	});
	const pathname = url.parse(req.url).pathname;
	const query = qs.parse(url.parse(req.url).query);
	const include = query.include ? query.include.split(',').sort() : [];
	const api = backend(options);
	api.fetchCompound(pathname, include, done);
});

const handler = (req, res, next) => {
	const start = (new Date()).getTime();
	const target = `${req.path}${(url.parse(req.originalUrl).search || '')}`;
	fetch(
		{
			url: target,
			headers:{ 'x-forwarded-host': req.headers['x-forwarded-host'] } 
		}, 
		(err, result) => {
			perf('fetched %s in %sms', target, (new Date()).getTime() - start);
			if(err || result.errors) {
				return next((result||{}).errors || err)		
			}
			res.set('Cache-Control', `public, max-age=${process.env.CACHE_CONTROL_MAXAGE || 60}`)
			return res.json(result);
		}
	);
}

module.exports = handler;
