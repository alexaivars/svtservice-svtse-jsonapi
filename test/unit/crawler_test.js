const assert = require('chai').assert;
const crawler = require('../../app/crawler');
const clone = obj => Object.freeze(Object.assign({}, obj));
const merge = (a, b) => Object.freeze(Object.assign({}, a, b));

const single = { "data": [{ "type": "single", "id": "1" }] };

const compound_1 = {
	"data": [{
		"type": "compound",
		"id": "compound_1",
		"relationships": {
			"related1": { "data": [{ "type": "resourceType1", "id": "type1_1" }] },
			"related2": { "data": [{ "type": "resourceType2", "id": "type2_1" }] },
			"related3": { "data": [{ "type": "resourceType3", "id": "type3_1" }] },
			"related4": { "data": [{ "type": "resourceType4", "id": "type4_1" }, { "type": "resourceType4", "id": "type4_2" }] },
			"broken":		{ "data": [{ "type": "error", "id": "404" }] },
			"broken2":		{ "data": [{ "type": "error", "id": "404" }] }
		}
	}]
};

const compound_error = {
	"data": [{
		"type": "compound",
		"id": "compound_error",
		"relationships": {
			"broken1":		{ "data": [{ "type": "error", "id": "broken1" }] },
			"broken2":		{ "data": [{ "type": "error", "id": "broken2" }] },
			"related": { "data": [{ "type": "resourceType1", "id": "type1_error" }] }
		}
	}]
};


const type1_1			= { "data": [{ "type": "resourceType1", "id": "type1_1" }] };
const type1_error	= { "data": [{ "type": "resourceType1", "id": "type1_error", "relationships": { "broken": { "data": [{ "type": "error", "id": "broken3" }, { "type": "error", "id": "broken4" }, { "type": "resourceType1", "id": "type1_1" }] } } }] };
const type2_1			= { "data": [{ "type": "resourceType2", "id": "type2_1" }] };
const type3_1			= { "data": [{ "type": "resourceType3", "id": "type3_1", "relationships": { "related3_1": { "data": [{ "type": "resourceType3", "id": "type3_1_1" }] } } }] };
const type3_1_1		= { "data": [{ "type": "resourceType3", "id": "type3_1_1", "relationships": { "related3_1_1": { "data": [{ "type": "resourceType3", "id": "type3_1_1_1" }] } } }] };
const type3_1_1_1	= { "data": [{ "type": "resourceType3", "id": "type3_1_1_1" }] };
const type4_1			= { "data": [{ "type": "resourceType4", "id": "type4_1" }] };
const type4_2			= { "data": [{ "type": "resourceType4", "id": "type4_2" }] };
const errors			= { "errors" : [{ "status": "404", source: { parameter: 'http://foo.io/error' } }] };
const invalid			= {
	data: [
		{
			id: "important",
			type: "message_lists",
			relationships: {
				messages: null
			}
		}
	]
};

const frontpage = {
  "data": [
    {
      "id": "frontpage",
      "type": "pages",
      "relationships": {
        "editorials": {
          "data": {
            "id": "frontpage_editorials",
            "type": "teaser_lists"
          }
        },
        "news": {
          "data": {
            "id": "frontpage_news",
            "type": "teaser_lists"
          }
        }
      }
    }
  ]
}

const frontpage_editorials = {
	"data": [
    {
      "id": "frontpage_editorials",
      "type": "teaser_lists",
      "relationships": {
        "teasers": {
          "data": [
            {
              "id": "4952614",
              "type": "teasers"
            },
            {
              "id": "4946032",
              "type": "teasers"
            }
          ]
        }
      }
    }
	]
}

const teasers_4952614 = {
	"data": [
    {
      "id": "4952614",
      "type": "teasers",
      "relationships": {
        "cover": {
          "data": {
            "id": "4940428",
            "type": "images"
          }
        }
      }
    }
	]
}

const teasers_4946032 = {
	"data": [
    {
      "id": "4946032",
      "type": "teasers",
      "relationships": {
        "cover": {
          "data": {
            "id": "4940428",
            "type": "images"
          }
        }
      }
    }
	]
}

const images_4940428 = {
	"data": [
    {
      "id": "4940428",
      "type": "images"
    }	
	]
}

function fetcher(path, done) {
	const url = `http://foo.io${path.split('?')[0]}`;
	switch(url) {
		case 'http://foo.io/compound/compound_1': return done(null, clone(compound_1));
		case 'http://foo.io/compound/compound_error': return done(null, clone(compound_error));
		case 'http://foo.io/resourceType1/type1_error': return done(null, clone(type1_error));
		case 'http://foo.io/resourceType1/type1_1': return done(null, clone(type1_1));
		case 'http://foo.io/resourceType2/type2_1': return done(null, clone(type2_1));
		case 'http://foo.io/resourceType3/type3_1': return done(null, clone(type3_1));
		case 'http://foo.io/resourceType3/type3_1_1': return done(null, clone(type3_1_1));
		case 'http://foo.io/resourceType3/type3_1_1_1': return done(null, clone(type3_1_1_1));
		case 'http://foo.io/resourceType4/type4_1': return done(null, clone(type4_1));
		case 'http://foo.io/resourceType4/type4_2': return done(null, clone(type4_2));
		case 'http://foo.io/single': return done(null, clone(single));
		case 'http://foo.io/error': return done(null, clone(errors));
		case 'http://foo.io/error/404': return done(null, clone(errors));
		case 'http://foo.io/error/broken1': return done(null, clone(errors));
		case 'http://foo.io/error/broken2': return done(null, clone(errors));
		case 'http://foo.io/error/broken3': return done(null, clone(errors));
		case 'http://foo.io/error/broken4': return done(null, clone(errors));
		case 'http://foo.io/invalid': return done(null, clone(invalid));
		case 'http://foo.io/frontpage': return done(null, clone(frontpage));
		case 'http://foo.io/teaser_lists/frontpage_editorials': return done(null, clone(frontpage_editorials));
		case 'http://foo.io/teasers/4952614': return done(null, clone(teasers_4952614));
		case 'http://foo.io/teasers/4946032': return done(null, clone(teasers_4946032));
		case 'http://foo.io/images/4940428': return done(null, clone(images_4940428));
		default:
			return done(null, clone(errors));
	}
}

describe('[unit] crawler', () => {
	
	const service = crawler(fetcher);
	it('should not include duplicate resources', done => {
		const expected = merge(frontpage, { 
			included:[]
				.concat(frontpage_editorials.data)
				.concat(teasers_4952614.data)
				.concat(teasers_4946032.data)
				.concat(images_4940428.data)
		});
		return service.fetchCompound('/frontpage', ['editorials', 'editorials.teasers', 'editorials.teasers.cover'], (err, actual) => {
			// console.log(JSON.stringify(actual, null, 2));
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});

	it('should proxy simple resource requests', done => {
		return service.fetch('/single', (err, actual) => {
			const expected = clone(single);
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});

	it('should resolve a single relationship', done => {
		const expected = merge(compound_1, { 
			included:[]
				.concat(type1_1.data)
		});
		return service.fetchCompound('/compound/compound_1', ['related1'], (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});
	
	it('should resolve multiple relationships', done => {
		const expected = merge(compound_1, { 
			included:[]
				.concat(type1_1.data)
				.concat(type2_1.data)
		});
		return service.fetchCompound('/compound/compound_1', ['related1', 'related2'], (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});
	
	it('should resolve single deep relationships', done => {
		const expected = merge(compound_1, { 
			included:[]
				.concat(type3_1_1_1.data)
		});
		return service.fetchCompound('/compound/compound_1', ['related3.related3_1.related3_1_1'], (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});
	
	it('should resolve multiple deep relationships', done => {
		const expected = merge(compound_1, { 
			included:[]
				.concat(type3_1.data)
				.concat(type3_1_1.data)
				.concat(type3_1_1_1.data)
		});
		return service.fetchCompound('/compound/compound_1', ['related3', 'related3.related3_1', 'related3.related3_1.related3_1_1'], (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});

	it('should resolve nested relationships', done => {
		const expected = merge(compound_1, { 
			included:[]
				.concat(type4_1.data)
				.concat(type4_2.data)
		});
		return service.fetchCompound('/compound/compound_1', ['related4'], (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});
	
	it('should handle simple errors', done => {
		const expected = clone(errors);
		return service.fetch('/error', (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});
	
	it('should only return a single error when the root resource is invalid', done => {
		const expected = clone(errors);
		return service.fetchCompound('/error/404', ['foo', 'foo.bar', 'foo.bar.baz'], (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});
	
	it('should report simple errors as failed in meta', done => {
		const expected = merge(compound_error, { 
			included:[]
				.concat(type1_1.data),
			meta: {
				failed: [
					{
						"source": {
							"parameter": "http://foo.io/error",
							"pointer": "/related/broken/broken3"
						},
						"status": "404"
					},
					{
						"source": {
							"parameter": "http://foo.io/error",
							"pointer": "/related/broken/broken4"
						},
						"status": "404"
					}
				]
			}
		});
		return service.fetchCompound('/compound/compound_error', ['related.broken'], (err, actual) => {
			assert.equal(err, null, 'without errors');
			assert.deepEqual(actual, expected);
			return done();			 
		})	
	});
	

});
