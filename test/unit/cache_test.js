const assert = require('chai').assert;
const sinon = require('sinon');
const cache = require('../../app/cache');
const series = require('async').series;
const state = {
	set: (key, value, ttl, done) => { setImmediate(() => done()); },
	get: (key, done) => { setImmediate(() => done()); },
	dump: (done) => { setImmediate(() => done()); }
}

describe('[unit] cache', () => {

	var set, get, dump, mem;
	beforeEach(() => {
		const hash = new Map();
		set = sinon.spy(state, 'set');
		get = sinon.spy(state, 'get');
		dump = sinon.spy(state, 'dump');
		mem = {
			set: (key, value, ttl, done) => { setImmediate(() => done(null, hash.set(key, value))); },
			get: (key, done) => { setImmediate(() => done(null, hash.get(key))); },
			dump: (done) => { setImmediate(() => done()); }
		}
	});

	afterEach(() => {
		set.restore();
		get.restore();
		dump.restore();
	});

	it('should not cache values with a invalid ttl', done => {
		const store = cache(state);
		series([
			done => store.set({ url:'foo.com' }, { meta: { maxage: 1 } }, done),
			done => store.set({ url:'not.com' }, { meta: { maxage: -1 } }, done),
			done => store.set({ url:'not.com' }, { meta: { maxage: 0 } }, done)
		], (/*err, results*/) => {
			const args = set.args[0];
			assert.isTrue(set.calledOnce);
			assert.strictEqual(args[0], 'foo.com');
			assert.strictEqual(args[2], 1);
			done();
		});
	});
	
	it('should retrieve values by key', done => {
		const store = cache(state);
		store.get({ url:'foo.com' }, (/*err, value*/) => {
			const args = get.args[0];
			assert.isTrue(get.calledOnce);
			assert.strictEqual(args[0], 'foo.com');
			done();
		});
	});
	
	it('should have header data as part of the key', done => {
		const store = cache(state);
		store.get({ url:'foo.com', headers: { ['x-forwarded-host']: 'fwd.io' } }, (/*err, value*/) => {
			const args = get.args[0];
			assert.isTrue(get.calledOnce);
			assert.strictEqual(args[0], 'foo.com#fwd.io');
			done();
		});
	});
	
	it('should not cache error reponses', done => {
		const store = cache(state);
		store.set({ url:'foo.com' }, { errors:[{ id:'foo' }] }, (/*err, value*/) => {
			assert.isTrue(set.notCalled);
			done();
		});
	});
	
	it('should not return a value when a error occures', done => {
		const store = cache({
			set: (key, value, ttl, done) => done(new Error('set error'), null),
			get: (key, done) => done(new Error('get errro'), null),
			dump: (done) => done(new Error('dump errro'), null)
		});
		store.get({ url:'foo.com' }, (err, value) => {
			assert.isNull(err, 'there was no error passed');
			assert.isNull(value, 'there was no value passed');
			done();
		});
	});
	
	it('should set and retrive values from it\'s state', done => {
		const store = cache(mem);
		const expected = Object.freeze({ id:'foo', type:'bar', data:[], meta: { maxage: 10 } });
		const req = { url:'foo.com' };
		series([
			done => store.set(req, expected, done),
			done => store.get(req, done)
		], (err, results) => {
			assert.deepEqual(expected, results[0]);
			assert.deepEqual(expected, results[1]);
			done();
		});
	});
	
	it('should not save values with zero maxage to it´s state', done => {
		const store = cache(mem);
		const expected = Object.freeze({ id:'foo', type:'bar', data:[], meta: { maxage: 0 } });
		const req = { url:'foo.com' };
		series([
			done => store.set(req, expected, done),
			done => store.get(req, done)
		], (err, results) => {
			assert.deepEqual(expected, results[0]);
			assert.isNull(results[1], 'there was no error passed');
			done();
		});
	});


});

