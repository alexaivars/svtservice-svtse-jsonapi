const assert = require('chai').assert;
const http = require('../../app/http');
const nock = require('nock');

describe('[unit] http', () => {
	
	it('should return json api error when resource is not found', done => {
		nock('http://foo.io')
		.get('/bar')
		.reply(404, 'not found');

		const expected = Object.freeze({ 
			"errors": [{
				"status": "404",
				"title": "Not Found",
				"source": { "parameter": "http://foo.io/bar" },
				"meta": { "source": { "code": "404", "error": "not found", "path": "http://foo.io/bar" } }
			}]
		});

		http.get({ url: 'http://foo.io/bar' }, (err, actual) => {
			assert.deepEqual(actual, expected);
			done();
		});
	});
	
	it('should return json api error when resource is not a jsonapi document', done => {
		nock('http://foo.io')
		.get('/bar')
		.reply(200, "not json");
		
		const expected = Object.freeze({ 
			"errors": [{
				"status": "415",
				"title": "Unsupported Media Type",
				"source": { "parameter": "http://foo.io/bar" },
				"meta": { "source": { "code": "200", "error": "not json", "path": "http://foo.io/bar" } }
			}]
		});

		http.get({ url: 'http://foo.io/bar' }, (err, actual) => {
			assert.deepEqual(actual, expected);
			done();
		});
	});
	
	it('should return valid jsonapi object', done => {
		nock('http://foo.io')
		.get('/bar')
		.reply(200, { "data": [{ "type":"foo", "id":"bar" }], "meta":{ "maxage":"0" } });
		
		const expected = Object.freeze({
												"data": [{ "id": "bar", "type": "foo" }],
												"meta": { "maxage": "0" }
											});

		http.get({ url: 'http://foo.io/bar' }, (err, actual) => {
			assert.deepEqual(actual, expected);
			done();
		});
	});
	
	it('should propagate max-age headers', done => {
		nock('http://foo.io')
		.get('/bar')
		.reply(
			200,
			{ "data": [{ "type":"foo", "id":"bar" }] },
			{ 'cache-control':'max-age=12345' }
		);
		
		const expected = Object.freeze({
												"data": [{ "id": "bar", "type": "foo" }],
												"meta": { "maxage": "12345" }
											});

		http.get({ url: 'http://foo.io/bar' }, (err, actual) => {
			assert.deepEqual(actual, expected);
			done();
		});
	});
	
	it('should propagate network errors', done => {
		nock('http://foo.io')
		.get('/bar')
		.replyWithError('random error');
		
		const expected = Object.freeze({ "errors": [{
      "status": "500",
			"title": "Network error",
			"source": { "parameter": "http://foo.io/bar" },
      "meta": { "source": { "path": "http://foo.io/bar", "error": "random error" } }
    }] });

		http.get({ url: 'http://foo.io/bar' }, (err, actual) => {
			assert.deepEqual(actual, expected);
			done();
		});
	});
	
	it('should propagate response errors', done => {
		nock('http://foo.io')
		.get('/bar')
		.reply(500, 'Server error');
										
		const expected = Object.freeze({ 
			"errors": [{
				"status": "500",
				"title": "Internal Server Error",
				"source": { "parameter": "http://foo.io/bar" },
				"meta": { "source": { "code": "500", "error": "Server error", "path": "http://foo.io/bar" } }
			}]
		});

		http.get({ url: 'http://foo.io/bar' }, (err, actual) => {
			assert.deepEqual(actual, expected);
			done();
		});
	});

});
