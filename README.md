# svtse jsonapi #

This project is a [JSONAPI](http://jsonapi.org) includes service that resolves resource relations and returns a single compound document.

### Requirements ###

* NodeJS >= 4.0.0 
* Redis

### Setup ###

* Get the code: `git clone git@bitbucket.org:svtidevelopers/svtservice-svtse-jsonapi.git`
* Install package dependencies: `npm install`
* Start the server: `node index`

### Configuration ###

The following environment variables can be used to configure how the service behaves

```
PORT = <port number> || '7000' (default)
HOSTNAME <x-forwarded-host> || 'localhost' (default)
REDISTOGO_URL <a redis to go url> || (redis defaults to localhost standard redis config)
CACHE_TYPE = 'none' || 'memory' || 'memory-redis' || 'redis' (default)
CACHE_CONTROL_MAXAGE = <Cache-Control max-age>
API_DEFAULT_MAX_AGE = <internal cache ttl used for in-memory and redis>
DEBUG = 'jsonapi:*' || 'jsonapi:*:error' || 'jsonapi:*:debug' || 'jsonapi:*:perf' || '' (default)  
```

*see [github.com/visionmedia/debug](https://github.com/visionmedia/debug) for more debug options.*
