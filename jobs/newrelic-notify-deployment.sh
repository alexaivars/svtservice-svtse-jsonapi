#!/usr/bin/env bash
set -e


function show_help {
    echo "usage: $(basename $0) -r REVISION -a HEROKU_APP_NAME -k NEWRELIC_API_KEY"
}

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
output_file=""
verbose=0

while getopts "hr:a:k:" opt; do
    case "$opt" in
    h) show_help
    exit 0
    ;;
    r) REVISION=$OPTARG
    ;;
    k) NEWRELIC_API_KEY=$OPTARG
    ;;
    a) HEROKU_APP=$OPTARG
    ;;
    esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift

if [[ -z "$REVISION" ]]; then
    echo "No revision specified."
    show_help
    exit 1;
fi

if [[ -z "$NEWRELIC_API_KEY" ]]; then
    echo "No newrelic API key specified."
    show_help
    exit 1;
fi

if [[ -z "$HEROKU_APP" ]]; then
    echo "Heroku app name is missing. Please run heroku apps to find it."
    show_help
    exit 1;
fi

DEPLOY_USER="$(whoami)" # TODO maybe have an override?
NEWRELIC_APP_ID="$(heroku config:get NEW_RELIC_APP_NAME -a ${HEROKU_APP})"

if [[ -z "$NEWRELIC_APP_ID" ]]; then
    echo "No newrelic api key is specified for the heroku app. Check NEW_RELIC_APP_NAME config variable for the application"
    exit 1;
fi



curl -H "x-api-key:${NEWRELIC_API_KEY}" -d "deployment[application_id]=${NEWRELIC_APP_ID}" -d "deployment[revision]=${REVISION}" -d "deployment[user]=${DEPLOY_USER}" https://api.newrelic.com/deployments.xml
