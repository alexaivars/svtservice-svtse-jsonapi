#!/usr/bin/env bash
set -e

function show_help {
    echo "usage: $(basename $0) -a HEROKU_SRC_APP_NAME"
}

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
output_file=""
verbose=0

while getopts "hr:a:" opt; do
    case "$opt" in
    h) show_help
    exit 0
    ;;
    a) HEROKU_APP=$OPTARG
    ;;
    esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift

if [[ -z "${HEROKU_APP}" ]]; then
    echo "The name of the heroku app to promote is missing. Please run heroku apps to find it."
    show_help
    exit 1;
fi



heroku pipelines:promote --app ${HEROKU_APP}
