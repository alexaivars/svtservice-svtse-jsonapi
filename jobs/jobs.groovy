/**
 * Job DSL script to set up the Jenkins jobs for this project.
 *
 * Find out more about how to write Job DSL scripts at: https://github.com/jenkinsci/job-dsl-plugin
 * The DSL Reference is hosted here: https://github.com/jenkinsci/job-dsl-plugin/wiki/Job-reference
 *
 *
 * Requires:
 * - CloudBees Folders Plugin
 * - Delivery Pipeline Plugin
 * - Slack Plugin
 * - and maybey more
 */

//
// Git configuration
//
gitUrl = 'git@bitbucket.org:svtidevelopers/svtservice-svtse-jsonapi.git'
defaultGitBranch = 'refs/heads/master'
gitCredentialsId = 'ca4e939a-e07b-4082-b8b2-58decb6a6619'

//
// Pipeline configuration
//
folderName = 'svtservice-svtse-jsonapi'
folderDisplayname = 'svtservice-svtse-jsonapi'
defaultViewName = 'Pipeline View'
pipelineDisplayName = 'SVTSE JSONAPI Pipeline'
initialJobName = '1-test'

testJobName = "${folderName}/1-test"
deployDevJobName = "${folderName}/2-deploy-dev"
smokeDevJobName = "${folderName}/3-smoke-dev"
promoteStageJob = "${folderName}/4-promote-stage"
smokeStageJobName = "${folderName}/5-smoke-stage"
promoteProductionJobName = "${folderName}/6-promote-production"


//
// Slack configuration
//
slackChannel = '#platform-alerts'

//
// Build and deploy configuration
//
herokuDev = 'svtservice-svtse-jsonapi-dev'
herokuStage = 'svtservice-svtse-jsonapi-stage'
herokuProd = 'svtservice-svtse-jsonapi'
NODE_VERSION = '5.1.1'
newrelicApiKey = '66c9276c90faef7ec73d030955aa2ca991a9aa72496da77'


folder(folderName) {
    displayName folderDisplayname
    primaryView(defaultViewName)
}

deliveryPipelineView("${folderName}/${defaultViewName}") {
    pipelineInstances(3)
    allowPipelineStart(true)
    sorting(Sorting.TITLE)
    updateInterval(5)
    enableManualTriggers(true)
    showAvatars()
    showChangeLog()
    pipelines {
        component(pipelineDisplayName, initialJobName)
    }
}


freeStyleJob("${folderName}/template") {
    logRotator(14, 20, 14, 20)
    checkoutRetryCount(5)

    wrappers {
        colorizeOutput('xterm')
        timestamps()

    }
    scm {
        git {
            remote {
                name('master')
                url(gitUrl)
                credentials(gitCredentialsId)
            }
            branch('*/master')
        }
    }

    publishers {
        slackNotifications {
            projectChannel(slackChannel)
            notifyBuildStart(false)
            notifyAborted(true)
            notifyFailure(true)
            notifyNotBuilt(true)
            notifySuccess(false)
            notifyUnstable(true)
            notifyBackToNormal(true)
            notifyRepeatedFailure(true)
            showCommitList(true)
            includeTestSummary(false)
        }
    }
}

freeStyleJob(testJobName) {
    using "${folderName}/template"
    deliveryPipelineConfiguration('development', 'Test')

    wrappers {
        buildName('#${BUILD_NUMBER}-${GIT_REVISION,length=7}')
    }

    triggers {
        scm('H/5 * * * *')
    }
    steps {
        shell("""
            #!/bin/bash -e
            set +x

            export NVM_DIR="/var/lib/jenkins/.nvm"
            [ -s "\$NVM_DIR/nvm.sh" ] && . "\$NVM_DIR/nvm.sh"
            nvm install $NODE_VERSION
            set -x

            npm prune
            npm install

            npm run test
            """)
    }
    publishers {
        downstreamParameterized {
            trigger(deployDevJobName) {
                condition('SUCCESS')
                parameters {
                    predefinedProp('GIT_REF', '$GIT_COMMIT')
                }
            }
        }
    }
}

freeStyleJob(deployDevJobName) {
    using "${folderName}/template"
    deliveryPipelineConfiguration('development', 'Deploy')

    steps {
        shell("""
            echo "pushing to heroku"
            jobs/heroku-deploy.sh -a ${herokuDev}

            echo "notifying newrelic"
            jobs/newrelic-notify-deployment.sh -r \${GIT_REF} -a ${herokuDev} -k ${newrelicApiKey}
            """)
    }

    publishers {
        downstreamParameterized {
            trigger(smokeDevJobName) {
                condition('SUCCESS')
                parameters {
                    predefinedProp('GIT_REF', '$GIT_COMMIT')
                }
            }
        }
    }
}

freeStyleJob(smokeDevJobName) {
    using "${folderName}/template"
    deliveryPipelineConfiguration('development', 'Smoke Test')

    wrappers {
        buildName('#${BUILD_NUMBER}-${GIT_REVISION,length=7}')
    }
    steps {
        shell("""
            jobs/heroku-smoketest.sh -a ${herokuDev}
        """)

    }
    publishers {
        downstreamParameterized {
            trigger(promoteStageJob) {
                condition('SUCCESS')
                parameters {
                    predefinedProp('GIT_REF', '$GIT_COMMIT')
                }
            }
        }
    }
}


freeStyleJob(promoteStageJob) {
    using "${folderName}/template"
    deliveryPipelineConfiguration('stage', 'Promote')

    wrappers {
        buildName('#${BUILD_NUMBER}-${GIT_REVISION,length=7}')
    }
    steps { // build step
        shell("""
            echo "promoting to heroku (dev -> stage)"
            jobs/heroku-promote.sh -a ${herokuDev}

            echo "notifying newrelic"
            jobs/newrelic-notify-deployment.sh -r \${GIT_REF} -a ${herokuStage} -k ${newrelicApiKey}
         """)

    }
    publishers {
        downstreamParameterized {
            trigger(smokeStageJobName) {
                condition('SUCCESS')
                parameters {
                    predefinedProp('GIT_REF', '$GIT_COMMIT')
                }
            }
        }
    }
}

freeStyleJob(smokeStageJobName) {
    using "${folderName}/template"
    deliveryPipelineConfiguration('stage', 'Smoke Test')

    wrappers {
        buildName('#${BUILD_NUMBER}-${GIT_REVISION,length=7}')
    }
    steps { // build step
        shell("""
            jobs/heroku-smoketest.sh -a ${herokuStage}
        """)

    }
    publishers {
        buildPipelineTrigger(promoteProductionJobName) {
                parameters {
                    predefinedProp('IMAGE_TAG', '$GIT_COMMIT')
                    predefinedProp('GIT_REF', '$GIT_COMMIT')
                }
        }
    }
}

freeStyleJob(promoteProductionJobName) {
    using "${folderName}/template"
    deliveryPipelineConfiguration('production', 'Promote')

    wrappers {
        buildName('#${BUILD_NUMBER}-${GIT_REVISION,length=7}')
    }
    steps { // build step
        shell("""
            echo "promoting to heroku (stage -> prod)"
            jobs/heroku-promote.sh -a ${herokuStage}

            echo "notifying newrelic"
            jobs/newrelic-notify-deployment.sh -r \${GIT_COMMIT} -a ${herokuProd} -k ${newrelicApiKey}
            """)
    }
}



