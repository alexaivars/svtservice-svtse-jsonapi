/* jshint undef: false */
/* eslint-env node */

/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
	ssl: false, // workaround how SSL certs are stored in memory. LEEK
							// https://discuss.newrelic.com/t/memory-leaking-only-with-node-js-agent-installed/14448/6
	port: 80,

	/**
	 * Array of application names.
	 */
	app_name : ['svtservice-svtse-jsonapi'], // Override with NEW_RELIC_APP_NAME
	/**
	 * Your New Relic license key.
	 */
	license_key : '', // Override with NEW_RELIC_LICENSE_KEY


	agent_enabled : 'false', // Override with NEW_RELIC_ENABLED

	logging : {
		/**
		 * Level at which to log. 'trace' is most useful to New Relic when diagnosing
		 * issues with the agent, 'info' and higher will impose the least overhead on
		 * production applications.
		 */
		level : 'info', // TODO return to info

		/**
		 * Where to put the log file (stdout is stdout)
		 */
		filepath : 'stdout'
	}
};
