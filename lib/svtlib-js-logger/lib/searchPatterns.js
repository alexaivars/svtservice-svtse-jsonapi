'use strict';

/**
 * Kibana RegExp patterns to be used when formatting
 * @type {{LOG_PATTERN: RegExp, COLORS: RegExp, TRIM: RegExp, LOG_LEVEL: RegExp, WHITESPACE: RegExp, DURATION: RegExp, CustomLogLevel: Function}}
 */
module.exports = {
    // Prefix date stamp: Tue, 25 Aug 2015 15:27:33 GMT
    UTC_DATE_STAMP: /^\s*[A-Za-z]{3}\,\s\d{2}\s[A-Za-z]{3}\s\d{4}\s\d{2}:\d{2}:\d{2}\sGMT/g,

    // Matches and separates domain and message
    LOG_PATTERN: /^\s*((?::?\w+|-)+) (.*)$/,

    // TODO Some sort of color matching!? To be explained :)
    COLORS: /\u001b(?:\[\??(?:\d\;*)*[A-HJKSTfminsulh])?/g,

    // Remove leading and trailing whitespaces
    TRIM: /^\s+|\s+$/g,

    // Find valid Kibana log levels
    LOG_LEVEL: /:(TRACE|DEBUG|INFO|WARN|ERROR|FATAL)$/gi,

    // Find whitespace characters (space, tab, newline etc)
    WHITESPACE: /\s+/g,

    // Find ending duration field. Format +|- <number> ms|s|h|d
    DURATION:/((?:\+|\-)[0-9]+\s*(?:ms|m|s|h|d))$/g,

    /**
     * Create RegExp object from custom level config object
     *
     * @param {object} levelsMap custom level to Kibana log level mapping object
     * @returns {RegExp|null} regexp matching the custom log levels, or null if non-existing or malformatted config object
     */
    customLogLevel: function (levelsMap) {
        var pattern = '';
        if (typeof levelsMap === 'object')
        for (var prop in levelsMap) {
            if (!levelsMap.hasOwnProperty(prop)) {
                continue;
            }

            if (pattern.length > 0) {
                pattern += '|'
            }

            pattern += prop.toUpperCase();
        }

        // Only return regexp if a pattern text string is present
        if (typeof pattern === 'string' && pattern.length) {
            return new RegExp(':(' + pattern + ')$', 'gi');
        } else {
            return null;
        }
    },

    /**
     * Check if level is a supported Kibana log level
     * @param {string} level the level to check if it conforms to the standard Kibana levels
     * @returns {boolean} true if matching Kibana levels
     */
    allowedLogLevel: function (level) {
        return level.match(/(TRACE|DEBUG|INFO|WARN|ERROR|FATAL)$/gi) !== null;
    }
};