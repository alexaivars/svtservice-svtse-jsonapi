'use strict';

/**
 *
 * @param {Object} config configuration object
 * @param {Object} config.customLogLevels Custom log level mapping to Kibana log level.
 *
 * Example confit:
 * {
 *      "customLogLevels": {"NEWLEVEL": "DEBUG"}
 * }
 * Maps NEWLEVEL to Kibana DEBUG level
 *
 * @constructor
 */
module.exports = function (config) {
    var customLogLevels;
    var Patterns = require('./searchPatterns');

    /**
     * If an Error object is passed to formatter it will be separated from the message string
     *
     * @param args
     * @returns The error object
     * @private
     */
    function _findErrorObject(args) {
        if (typeof args === 'string') {
            return undefined;
        }

        for (var i = 0; i < args.length; i++) {
            var arg = args[i];

            if (typeof arg === 'object' && arg.stack) {
                return arg;
            }
        }
    }

    /**
     * Returns a copy without an Error object
     *
     * @param args
     * @returns {Array} arguments
     * @private
     */
    function _removeErrorObject(args) {
        if (typeof args === 'string') {
            return args;
        }

        var result = [];
        for (var i = 0; i < args.length; i++) {
            var arg = args[i];

            if (typeof arg === 'object' && arg.stack) {
                continue;
            }

            result.push(arg);
        }

        return result;
    }

    /**
     * Remove colors, trim and replace whitespace with space
     *
     * @param {string|Array} message
     * @returns {string} cleaned message
     *
     * @private
     */
    function _cleanUp(message) {
        if (typeof message === 'string') {
            message = [message];
        }

        return require('util').format.apply({}, message).replace(Patterns.COLORS, '').replace(Patterns.TRIM, '').replace(Patterns.WHITESPACE, ' ');
    }

    /**
     * Remove prefix datestamp
     *
     * @param {string|Array} message
     * @returns {string} cleaned message
     *
     * @private
     */
    function _cleanUpUTCDateStamp(message) {
        if (typeof message === 'string') {
            message = [message];
        }

        return require('util').format.apply({}, message).replace(Patterns.UTC_DATE_STAMP, '');
    }

    /**
     * Finds duration if it's specified, else; return the default value of +0ms
     *
     * @param {string} message
     * @returns {string} duration
     *
     * @private
     */
    function _findDuration(message) {
        var durationMatch = message.match(Patterns.DURATION);

        var duration = '+0ms';
        if (durationMatch) {
            duration = durationMatch[0];
        }

        return duration;
    }

    /**
     * Finds duration and returns a copy where it's removed from the string
     *
     * @param {string} message
     * @returns {string}
     *
     * @private
     */
    function _removeDuration(message) {
        return message.replace(Patterns.DURATION, '').replace(Patterns.TRIM, '');
    }

    /**
     * Splits namespace/logger name and message from the 'message' string
     *
     * @param message
     * @returns {Array} namespace/logger name and message
     *
     * @private
     */
    function _splitNamespaceAndMessage(message) {
        var matches = message.match(Patterns.LOG_PATTERN);

        if (!matches && typeof matches === 'object' && matches.length < 2) {
           throw "Unable to match log format for message: " + message;
        }

        return matches;
    }

    /**
     * Finds log level it's specified, else; return default value of TRACE
     *
     * @param {string} logger logger name + debug level string
     * @returns {String} debug level
     *
     * @private
     */
    function _findLogLevel(logger) {
        var level = 'TRACE';

        // Extract log level from logger name. If non-existing use default TRACE
        var levelMatch = Patterns.LOG_LEVEL.exec(logger);
        if (levelMatch) {
            level = levelMatch[1].toUpperCase();
        // If custom log levels are supplied, use mapping to Kibana log level
        } else if (typeof customLogLevels === 'object') {
            levelMatch = Patterns.customLogLevel(customLogLevels).exec(logger);

            if (levelMatch) {
                var key = levelMatch[1].toUpperCase();
                if (customLogLevels.hasOwnProperty(key)) {
                    level = customLogLevels[key];
                }

                // If mapping to supported Kibana log level exist, use it otherwise use default
                level = Patterns.allowedLogLevel(level) ? level.toUpperCase() : 'TRACE';
            }
        }

        return level;
    }

    /**
     * Finds log level and returns a copy where it's removed from the string
     *
     * @param {String} logger
     * @returns {String}
     *
     * @private
     */
    function _removeLogLevelFromLoggerName(logger) {
        var result = logger.replace(Patterns.LOG_LEVEL, '');
        return result.replace(Patterns.customLogLevel(customLogLevels), '');

    }

    // Only set customLogLevels if there is an optional config object supplied
    if (config) {
        customLogLevels = config.customLogLevels;
    }

    return {
        /**
         * Takes a string formatted by the 'debug' library and converts it to a key/value object
         *
         * @param {String|Array} rawMessage A 'debug' formatted string
         * @param {boolean} useColors Are we to handle colors (and also remove timestamp as this is prepended on message when colors are not present for some weird reason)
         *
         * @returns {{time: string, level: string, logger: string, message: string, duration: string}}
         */
        format: function (rawMessage, useColors) {
            var errorObject = _findErrorObject(rawMessage);
            rawMessage = _removeErrorObject(rawMessage);

            var cleanedRawMessage = _cleanUp(rawMessage);

            // Debug module adds timestamp when not using colors for some unknown reason
            if (!useColors) {
                cleanedRawMessage = _cleanUpUTCDateStamp(cleanedRawMessage);
            }

            var duration = _findDuration(cleanedRawMessage);
            cleanedRawMessage = _removeDuration(cleanedRawMessage);

            var matches = _splitNamespaceAndMessage(cleanedRawMessage);
            var logger = matches[1];

            var level = _findLogLevel(logger);
            logger = _removeLogLevelFromLoggerName(logger);

            var message = matches[2];
            var time = (new Date()).toISOString();

            // make sure we do not break the key=value format if the string contains quotes
            message = message.replace(/"/g, '\\"');

            var logObj = {
                time: time,
                level: level,
                message: message,
                duration: duration,
                logger: logger
            };

            if (errorObject) {
                logObj.stacktrace = _cleanUp(errorObject.stack);
            }

            return logObj;
        }
    }
};