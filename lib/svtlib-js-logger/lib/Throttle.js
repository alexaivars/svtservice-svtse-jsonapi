'use strict';

/**
 * Create a new Throttle instance.
 * @param {number} The maximum number of request to allow
 * @param {number} timeInterval The interval in which the requested limit is valid for
 * @constructor
 */
function Throttle(limit, timeInterval) {
    var _this = this;

    _this._limit = limit;
    _this._timeInterval = timeInterval;

    _this._activeEpocInterval = -1;
    _this._requestsRemaining = _this._limit;
}

var prototype = Throttle.prototype;

/**
 * Check whether the rate is limited or not.
 *
 * @returns {boolean} true if the rate is limited
 */
prototype.isRateLimited = function () {
    var _this = this;
    var rateIsLimited = false;
    var currentEpocSec = _this._currentEpocSec();

    if (_this._activeEpocInterval === currentEpocSec) {
        rateIsLimited = _this._calcRemainingRequests();
    } else {
        _this._resetThrottling(currentEpocSec);
    }

    return rateIsLimited;
};

/**
 * Calculate the remaining requests within throttled interval
 * @returns {boolean} returns true if there are requests left
 * @private
 */
prototype._calcRemainingRequests = function () {
    var _this = this;
    _this._requestsRemaining--;
    return _this._requestsRemaining <= 0;
};

/**
 * Reset throttling interval
 * @param {number} currentEpocSec epoc time in seconds to reset to
 * @private
 */
prototype._resetThrottling = function (currentEpocSec) {
    var _this = this;
    _this._activeEpocInterval = currentEpocSec;
    _this._requestsRemaining = _this._limit;
};

/**
 * Get the current time in Epoc seconds
 * @returns {number} the epoc time in seconds
 * @private
 */
prototype._currentEpocSec = function () {
    var time = (new Date()).getTime();
    return Math.floor(time / this._timeInterval);
};


module.exports = Throttle;