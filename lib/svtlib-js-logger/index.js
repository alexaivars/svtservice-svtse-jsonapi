"use strict";

var KibanaLogFormatter = require('./lib/KibanaLogFormatter');

//TODO Implement throttling for the server?

/**
 *
 * @param {Object} config configuration object
 * @param {Object} config.customLogLevels Custom log level mapping to Kibana log levels
 *
 */
module.exports = function (config) {
    var Debug = require('debug');
    var logFormatter = new KibanaLogFormatter(config);

    Debug.log = function() {
        try {
            var logProperties = logFormatter.format(arguments, Debug.useColors());

            var stacktrace = '';
            if (logProperties.stacktrace) {
                stacktrace = ' stacktrace=' + JSON.stringify(logProperties.stacktrace);
            }

            this.log('time="%s" level="%s" logger="%s" message="%s" log_duration="%s"' + stacktrace, logProperties.time, logProperties.level, logProperties.logger, logProperties.message, logProperties.duration);
        } catch (e) {
            // If we cannot format log message we log with the svtlib-js-logger as "source"/logger
            this.log('time="%s" level="ERROR" logger="svtlib-js-logger" message="Log format exception: %s", %s', (new Date()).toISOString(), e, e.stack);
        }
    }.bind(console);
};